# 問題集


<br><br><br>

## 寫 API 時會大量使用 EntityFrameworkCore 與 Database 做溝通

1. Deffered Execution 是什麼意思?
    
2. IQueryable & IEnumerable 都提供 Deffered Execution 的服務, 但為什麼與 DbContext 做溝通實選擇 IQueryable 比較好呢?

3. LINQ 裡面的 Where Clause 其實是一個 IEnumerable(IQueryable) 帶有一個 Func<TSourc, bool> Delegate 當參數的 Exetension method, 請問當時做這些 exetention method 實,要在 return 前面加上什麼關鍵字, 才能使這個 exetension method 呈現 Deffered execution 的狀態?



<br><br><br>

## 實作 REST 時, 因為要完成 data shaping 的功能, Data Transfer Object 有時候必須動態生成

4. 請解釋 var & dynamic 的差異?


5. ExpandoObject & dynaimc 的差異?



<br><br><br>

## 當處理檔案串流時, 記憶體管理變得十分重要

6. 通常在什麼情況下要使用 using statement?


7. 什麼是 unmanaged resource?




<br><br><br>

## DotNetCore Web Framework 

8. DotNet Core Web App 裡面大量的使用到了 Dependency Injection，其中它提供了三種 method 做 dependency injection。分別是 AddSingleton, AddScoped & AddTrasient。請問用這三種方式注入 service 有什麼區別?

9. 說到 Data Validation 以會怎麼做?
    

10. Repository Pattern

    ![image](./img/Repository.png)




<br><br><br>

## Backend Structure

- Microservice

    ![image](./img/Microservice.png)

- Serveless

    ![image](./img/serverless.png)

